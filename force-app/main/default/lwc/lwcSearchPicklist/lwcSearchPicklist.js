import {
    LightningElement,
    track,
    api
 } from 'lwc';
import findRecords from '@salesforce/apex/CustomSearchPicklistController.searchPicklist';

export default class LwcSearchPicklist extends LightningElement {
    @track _disabled = false;
    @api inputPlaceholder = "Type to Search";
    @api isrequired = false;
    @api recordnotfound;
    @api iconname = "standard:account";
    @api objectName;
    @api searchfield;
    @api fieldlabel;
    @api filtercondition = '';
    @api filterfields = 'Id ';
    @api fieldName = '';
    @track searchInput;
  
    @track _fieldValue = '';
  
    @track _divClass = "slds-box slds-box_x-small slds-media slds-media_center input-selected ";
    @api inputStyle = '';
  
    @track inputClass = '';
    defaulInputtClass = 'slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right input-container';
  
    isOpen = false;
    handlersAdded = false;
    hasFocus = false;
  
    @track recordList = [];
    @track isListEmpty;
    @track selectedRecord;
    @track showError = false;
    @track isRecordSelected = false;
  
    @track sobjectClass = "slds-input lookup-input slds-combobox__input";
  
    recordSelected = false;
  
    @api
    get disabled() {
        return this._disabled
    }
    set disabled(x) {
        this._disabled = x;
    }
  
  
    @api
    get fieldValue() {
        console.log('this._fieldValue fieldName', this._fieldValue, this.fieldName);
        if (this._fieldValue) {
            this.searchInput = this._fieldValue;
            this.renderCity();
        }
  
        return this._fieldValue;
    }
   
   
    set fieldValue(x) {
        this._fieldValue = x;
    }
    get divClass() {
        return this._divClass + this.inputStyle + (this.disabled ? ' disabledTxt ' : '');
    }
  
    @api
    renderCity() {
       
        console.log('/render city, fieldName ', this.searchInput, this.fieldName);
        this.selectedRecord = this.searchInput;
        //this.inputClass = this.inputClass + ' input-hidden';
        this.inputClass = this.defaulInputtClass + ' input-hidden';
        console.log('this.inputClass renderCity', this.inputClass);
    }
  
    connectedCallback() {
        this.searchInput = this.fieldValue;
        console.log('this.fieldValue fieldName', this.searchInput, this.fieldName);
        if (this.searchInput ) {
            console.log('--------search input if block -----');
            this.renderCity();
            console.log('connected IF');
        }else{
            this.inputClass = this.defaulInputtClass;
            console.log('connected else');
        }
        console.log('this.inputClass connectedCallback', this.inputClass);
    }
  
   
  
  
  
    // dispatch selected record Event
    dispatchSelectedRecordEvent = _ => {
        if (this.selectedRecord) {
            this.template.querySelector(".input-container").classList.add("input-hidden");
        } else {
            this.template.querySelector(".input-container").classList.remove("input-hidden");
            this.template.querySelector("input").focus();
        }
        
        console.log('this.selectedRecord', this.selectedRecord, 'this.fieldName', this.fieldName);
        const selectedRecordEvent = new CustomEvent("selectedrec", {
            detail: {
                selectedRecord: this.selectedRecord,
                fieldName: this.fieldName
            }
        });
        this.dispatchEvent(selectedRecordEvent);
    };
  
    // get Required List
    fetchRecordList = _ => {
        if (this.selectedRecord) {
            return;
        }
  
        let searchKey = this.template.querySelector("input").value;
        if (searchKey.trim().length <= 1) {
            this.template.querySelector(".slds-combobox").classList.remove("slds-is-open");
            this.isOpen = false;
            return;
        }
        if (this.recordSelected === true) {
            return;
        }
  
        findRecords({
            searchKey: searchKey,
            objectName: this.objectName,
            searchField: this.searchfield
        }).then(response => {
            console.log('response>>>' + JSON.stringify(response));
            if (this.isRecordSelected === false) {
                this.recordList = [];
                for (let i = 0; i < response.length; i++) {
                    const rec = response[i];
                    this.recordList.push(rec);
                }
                this.template.querySelector(".slds-combobox").classList.add("slds-is-open");
                this.isOpen = true;
                this.isListEmpty = !(this.recordList.length > 0);
            }
        }).catch(error => {
            console.log('error>>>' + JSON.stringify(error));
            console.log("Error: " + (error.message || error.body.message));
        });
    };
  
    /**
     * Various UI Handlers for handling the Combobox hide and show events
     */
  
    selectRecord = event => {
        this.recordSelected = true;
        this.isRecordSelected = true;
        console.log('city: ', event.currentTarget.dataset.name);
        let name = event.currentTarget.dataset.name;
        this.template.querySelector("input").value = '';
        this.searchInput = '';
        this.recordList.some(selectedRecordData => {
            console.log('selectedRecordData', selectedRecordData, ' name', name);
            if (selectedRecordData === name) {
                this.selectedRecord = selectedRecordData;
                this.template.querySelector(".slds-combobox").classList.remove("slds-is-open");
                this.isOpen = false;
                this.dispatchSelectedRecordEvent();
                this.showError = false;
                return true;
            }
  
            return false;
        });
        event.preventDefault();
    };
  
    @api
    validateInput() {
        console.log("isValid");
        if (this.isrequired) {
            if (this.selectedRecord == null) {
                this.sobjectClass = "slds-input lookup-input slds-combobox__input slds-has-error error";
                this.showError = true;
                return false;
            } else {
                this.sobjectClass = "slds-input lookup-input slds-combobox__input";
                this.showError = false;
                return true;
            }
        } else {
            return true;
        }
    }
  
    deselectRecord = event => {
        this.isRecordSelected = false;
        this.inputClass = this.defaulInputtClass;
        if (this.disabled) {
            return;
        }
        this.selectedRecord = null;
        this.template.querySelector("input").value = "";
        this.fetchRecordList();
        if (this.isrequired) {
            this.sobjectClass = "slds-input lookup-input slds-combobox__input slds-has-error error";
            this.showError = true;
        } else {
            this.sobjectClass = "slds-input lookup-input slds-combobox__input";
            this.showError = false;
        }
        this.dispatchSelectedRecordEvent();
        console.log('this.inputClass deselectRecord', this.inputClass);
    };
  
    inputFocus(event) {
        this.recordSelected = false;
    }
  
    // handle key up event in input
    inputKeyUp = event => {
  
        let keyCode = event.keyCode;
        this.recordnotfound = event.target.value;
        this.fetchRecordList();
    };
  
    @api
    getrecordnotfound() {
        console.log('this.recordnotfound>>' + this.recordnotfound);
        return this.recordnotfound;
    }
  
    // handle refresh event
    handleInputClicked = event => {
        event.preventDefault();
        this.fetchRecordList();
    };
  
    hideCombobox = event => {
        if (event.currentTarget === event.target) {
            this.template.querySelector(".slds-combobox").classList.remove("slds-is-open");
        }
    };
  
    comboboxDropdownClicked = event => {
        event.preventDefault();
    };
  
    renderedCallback() {
        if (!this.handlersAdded) {
            this.template.querySelector(".slds-dropdown").addEventListener("focusout", this.hideCombobox);
            this.template.querySelector(".slds-dropdown").addEventListener("focusin", _ => {
                this.hasFocus = true;
            });
            this.template.querySelector("input").addEventListener("blur", event => {
                if (this.isOpen) {
                    this.hasFocus = false;
                    setTimeout(_ => {
                        if (!this.hasFocus) {
                            this.template.querySelector(".slds-combobox").classList.remove("slds-is-open");
                        }
                    }, 0);
                }
            });
            this.handlersAdded = true;
        }
    }
 }