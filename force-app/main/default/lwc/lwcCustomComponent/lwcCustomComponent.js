import {
    LightningElement,
    track,
    api,
    wire
  } from 'lwc';
  import {
    showToast,
    getErrorMessage, isValid
  } from "c/pubsub";

export default class LwcCustomComponent extends LightningElement {
    @api caseId;
    @api coverageId;
    @api isSubmitted;
    @api isContingent;
    @api beneficiaryId;
  
    //track Variables
    @track showPersonData = false;
    @track showOrganisationData = false;
    @track showTrustData = false;
    @track showbeneficiaryStatus;
    @track beneficiaryObj = {};
    @track showUpdate = false;
    @track isRequired = true;
    @track contactObj = {};
    @track isDisabled = false;
  
    @track obj = {
      "BeneficiaryClassification": '',
      "personFirstName": '',
      "personMidleName": '',
      "personLastName": '',
      "personRelationship": '',
      "persondateOfBirth": '',
      "personSocialSecurity": '',
      "streetAddress": '',
      "streetAddress2": '',
      "streetAddress3": '',
      "city": '',
      "state": '',
      "zipCode": '',
      "beneficiaryStatus": '',
      "organizationTrustName": '',
      "beneficiaryStatus": ''
    };
  
    get beneficiaryStatusOptions() {
      return [{
        label: "Primary",
        value: "Primary"
      },
      {
        label: "Contingent",
        value: "Contingent"
      }
      ];
    }
    @track maxDate;
  
    connectedCallback() {
      if(this.isSubmitted){
          this.isDisabled=true;
      }
      this.maxDate = new Date().toISOString().split("T")[0];  
  
      if (this.isContingent) {
        this.beneficiaryObj.beneficiaryStatus = 'Contingent';
      } else {
        this.beneficiaryObj.beneficiaryStatus = 'Primary';
  
      }
  
      if (this.beneficiaryId) {
        this.showUpdate = true;
      } else {
        this.showUpdate = false;
      }
  
    }

    handleInputChange(event) {
        this.beneficiaryObj[event.target.name] = event.target.value;
        this.beneficiaryObj[event.target.name] = this.beneficiaryObj[event.target.name].trim();
        if (event.target.value === 'Person') {
          this.showPersonData = true;
          this.showOrganisationData = false;
          this.showTrustData = false;
          this.isDisabled = false;
        } else if (event.target.value === 'Organization/Trust') {
          this.showOrganisationData = true;
          this.showPersonData = false;
          this.showTrustData = false;
          this.isDisabled = false;
        }
    
        if (event.target.name == 'phone') {
          this.handleOnBlur(event);
        }
      }
    
      //Used for Formatting the Phone Number
      handleOnBlur(event) {
        if (event.target.name == 'phone') {
          let ph = event.target.value;
          if (ph.length >= 10) {
            this.beneficiaryObj[event.target.name] = this.phoneFormat(ph);
          }
        }
      }
    
      get beneficiaryOptions() {
        return [{
          label: "Person",
          value: "Person"
        },
        {
          label: "Organization/Trust",
          value: "Organization/Trust"
        },
        ];
      }
    
      //To Populate Address from Contact
      changeHandler(event) {
        if (event.target.checked) {
          console.log('inside');
    
          this.beneficiaryObj.streetAddress = this.contactObj.Home_Street_Address_1__c;
          this.beneficiaryObj.streetAddress2 = this.contactObj.Home_Street_Address_2__c;
          this.beneficiaryObj.streetAddress3 = this.contactObj.Home_Street_Address_3__c;
          this.beneficiaryObj.city = this.contactObj.OtherCity;
          this.beneficiaryObj.state = this.contactObj.OtherState;
          this.beneficiaryObj.zipCode = this.contactObj.OtherPostalCode;
    
          setTimeout(function () {
            this.isValid('lightning-input');
            this.isDisabled = true;
          }.bind(this), 100);
    
        } else {
          this.beneficiaryObj.streetAddress = '';
          this.beneficiaryObj.streetAddress2 = '';
          this.beneficiaryObj.streetAddress3 = '';
          this.beneficiaryObj.city = '';
          this.beneficiaryObj.state = '';
          this.beneficiaryObj.zipCode = '';
          this.isDisabled = false;
    
        }
      }

      isValid(selector) {
        let valid = false;
        let isAllValid = [...this.template.querySelectorAll(selector)].reduce((validSoFar, input) => {
          input.reportValidity();
          return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        return valid;
      }
    
      phoneFormat(phone) {
        //format phone in 222-222-2222
        let ph = '';
        if (phone) {
          ph = phone.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
        }
        return ph;
      }
    
      ssnFormat(ssn) {
        //format ssn in 222-22-2222
        let ph = '';
        if (ssn) {
          ph = ssn.replace(/(\d{3})(\d{2})(\d{4})/, "$1-$2-$3");
        }
        return ph;
      }
    
      handleOnBlurForSSN(event) {
        if (event.target.name == 'personSocialSecurity') {
          let ssn = event.target.value;
          if (ssn.length >= 9) {
            this.beneficiaryObj.personSocialSecurity = this.ssnFormat(ssn);
          }
    
        }
      }
    
      //Gets data from Search Picklist Cmp for Relationship
      selectPicklist(event) {
        console.log('this.obj', JSON.stringify(this.beneficiaryObj));
        if (event.detail.selectedRecord) {
          this.beneficiaryObj[event.detail.fieldName] = event.detail.selectedRecord;
        } else {
          this.beneficiaryObj[event.detail.fieldName] = null;
        }
        console.log('this.obj2', JSON.stringify(this.beneficiaryObj));
      }
    
      //Validates Search Picklist
      validatePicklist() {
        let selectPicklist = [...this.template.querySelectorAll("c-gic-search-picklist")];
        let isAllValid = true;
        if (selectPicklist) {
          for (var i = 0; i < selectPicklist.length; i++) {
            let valid = selectPicklist[i].validateInput();
            if (!valid) {
              isAllValid = false;
            }
          }
        }
        console.log("isAllValid  for city", isAllValid);
        return isAllValid;
      }
}