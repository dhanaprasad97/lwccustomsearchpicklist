public with sharing class CustomSearchPicklistController {
      /*
    * Method Name: searchPicklist
    * Purpose: Get the picklist values of field on an object which matches searchKey
    * Parameter: searchKey, objectName, searchField
    * Return: List<String> 
    */ 
    
    @AuraEnabled
    public static List<String> searchPicklist(String searchKey,  String objectName, String searchField) {
    
        List<String> picklistValues = fetchPicklistValues(objectName,  searchField);
        List<String> searchResults = new List<String>();
        for(String picklistValue : picklistValues){
            System.debug('--picklistValue--> '+picklistValue + ' ---search key --> ' + searchKey);
        
            if(picklistValue.containsIgnoreCase(searchKey)){
                searchResults.add(picklistValue);
            }
        }
        return searchResults;
    }

   /*
    * Method Name: fetchPicklistValues
    * Purpose: Get the picklist values of field on an object
    * Parameter: objectName, fieldName
    * Return: List<String> 
    */ 

    @AuraEnabled
    public static List<String> fetchPicklistValues(String objectName, String fieldName){
       List<String> opts = new List<String>();
      
       Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
       Schema.DescribeSObjectResult r = s.getDescribe() ;
       Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
       Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      
       for( Schema.PicklistEntry pickListVal : ple){
           opts.add( pickListVal.getValue());
       }   
      
       return opts;
    }

}
