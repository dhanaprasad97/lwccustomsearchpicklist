declare module "@salesforce/apex/CustomSearchPicklistController.searchPicklist" {
  export default function searchPicklist(param: {searchKey: any, objectName: any, searchField: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomSearchPicklistController.fetchPicklistValues" {
  export default function fetchPicklistValues(param: {objectName: any, fieldName: any}): Promise<any>;
}
